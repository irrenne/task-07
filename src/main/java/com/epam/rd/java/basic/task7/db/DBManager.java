package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.*;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

    private static final String APP_CONFIG_PATH = "app.properties";
    private static final String CONNECTION_URL = "connection.url";

    private static DBManager instance;

    public static synchronized DBManager getInstance() {
        if (instance == null) {
            instance = new DBManager();
        }
        return instance;
    }

    private DBManager() {
    }

    public Connection getConnection() throws DBException {
        Properties appProps = new Properties();
        Connection connection = null;

        try {
            appProps.load(new FileInputStream(APP_CONFIG_PATH));
            String URL = appProps.getProperty(CONNECTION_URL);
            connection = DriverManager.getConnection(URL);
        } catch (SQLException | IOException e) {
            throw new DBException("fail to get connection", e);
        }
        return connection;
    }

    private static void close(AutoCloseable ac) {
        if (ac != null) {
            try {
                ac.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void rollback(Connection connection) throws DBException {
        if (connection != null) {
            try {
                connection.rollback();
            } catch (SQLException e) {
                throw new DBException("rollback fails ", e);
            }
        }
    }

    public List<User> findAllUsers() throws DBException {
        List<User> users = new ArrayList<>();
        Connection connection = getConnection();
        Statement statement = null;
        ResultSet rs = null;

        try {
            statement = connection.createStatement();
            rs = statement.executeQuery("SELECT * FROM users");
            while (rs.next()) {
                User user = new User();
                user.setId(rs.getInt("id"));
                user.setLogin(rs.getString("login"));
                users.add(user);
            }
        } catch (SQLException throwables) {
            throw new DBException("failed to get users", throwables);
        } finally {
            close(statement);
            close(rs);
            close(connection);
        }

        return users;
    }

    public boolean insertUser(User user) throws DBException {
        Connection connection = getConnection();
        String query = "INSERT INTO users (login) VALUES (?)";
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            ps = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, user.getLogin());
            ps.executeUpdate();
            rs = ps.getGeneratedKeys();
            if (rs.next()) {
                int userId = rs.getInt(1);
                user.setId(userId);
            }
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close(ps);
            close(rs);
            close(connection);
        }
        return false;
    }

    public boolean deleteUsers(User... users) throws DBException {
        List<Integer> ids = new ArrayList<>();
        for (User u : users) {
            ids.add(u.getId());
        }
        String idsStr = ids.toString();
        idsStr = idsStr.replace('[', '(').replace(']', ')');

        Connection connection = getConnection();
        String query = "DELETE FROM users WHERE id IN " + idsStr;
        PreparedStatement ps = null;

        try {
            ps = connection.prepareStatement(query);
            ps.executeUpdate();
            return true;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            close(ps);
            close(connection);
        }
        return false;
    }

    public User getUser(String login) throws DBException {
        String query = "SELECT id, login FROM users WHERE login=?";
        User user = new User();
        Connection connection = getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            ps = connection.prepareStatement(query);
            ps.setString(1, login);
            rs = ps.executeQuery();
            while (rs.next()) {
                user.setId(rs.getInt("id"));
                user.setLogin(rs.getString("login"));
            }
        } catch (SQLException throwables) {
            throw new DBException("failed to get user", throwables);
        } finally {
            close(ps);
            close(rs);
            close(connection);
        }

        return user;
    }

    public Team getTeam(String name) throws DBException {
        String query = "SELECT id, name FROM teams WHERE name=?";
        Team team = new Team();
        Connection connection = getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            ps = connection.prepareStatement(query);
            ps.setString(1, name);
            rs = ps.executeQuery();
            while (rs.next()) {
                team.setId(rs.getInt("id"));
                team.setName(rs.getString("name"));
            }

        } catch (SQLException throwables) {
            throw new DBException("failed to get team", throwables);
        } finally {
            close(ps);
            close(rs);
            close(connection);
        }

        return team;
    }

    public List<Team> findAllTeams() throws DBException {
        List<Team> teams = new ArrayList<>();
        Connection connection = getConnection();
        Statement statement = null;
        ResultSet rs = null;

        try {
            statement = connection.createStatement();
            rs = statement.executeQuery("SELECT * FROM teams");

            while (rs.next()) {
                Team team = new Team();
                team.setId(rs.getInt("id"));
                team.setName(rs.getString("name"));
                teams.add(team);
            }
        } catch (SQLException throwables) {
            throw new DBException("failed to get teams", throwables);
        } finally {
            close(statement);
            close(rs);
            close(connection);
        }
        return teams;
    }

    public boolean insertTeam(Team team) throws DBException {
        Connection connection = getConnection();
        String query = "INSERT INTO teams (name) VALUES (?)";
        PreparedStatement ps = null;

        try {
            ps = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, team.getName());
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next()) {
                int userId = rs.getInt(1);
                team.setId(userId);
            }
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close(ps);
            close(connection);
        }
        return false;
    }

    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
        Connection connection = getConnection();
        String query = "INSERT INTO users_teams (user_id, team_id) VALUES(?, ?)";
        PreparedStatement ps = null;

        try {
            connection.setAutoCommit(false);
            ps = connection.prepareStatement(query);
            for (Team t : teams) {
                ps.setInt(1, user.getId());
                ps.setInt(2, t.getId());
                ps.executeUpdate();
            }
            connection.commit();
            return true;
        } catch (SQLException throwables) {
            rollback(connection);
            throw new DBException("transaction failed", throwables);
        } finally {
            close(ps);
            close(connection);
        }
    }

    public List<Team> getUserTeams(User user) throws DBException {
        List<Team> userTeams = new ArrayList<>();
        Connection connection = getConnection();
        String query = "SELECT * FROM teams INNER JOIN users_teams ON teams.id = users_teams.team_id WHERE users_teams.user_id=?";
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            ps = connection.prepareStatement(query);
            ps.setInt(1, getUser(user.getLogin()).getId());
            rs = ps.executeQuery();
            while (rs.next()) {
                Team team = new Team();
                team.setId(rs.getInt("id"));
                team.setName(rs.getString("name"));
                userTeams.add(team);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            close(ps);
            close(rs);
            close(connection);
        }

        return userTeams;
    }

    public boolean deleteTeam(Team team) throws DBException {
        Connection connection = getConnection();
        String query = "DELETE FROM teams WHERE id=?";
        PreparedStatement ps = null;

        try {
            ps = connection.prepareStatement(query);
            ps.setInt(1, team.getId());
            ps.executeUpdate();
            return true;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            close(ps);
            close(connection);
        }
        return false;
    }

    public boolean updateTeam(Team team) throws DBException {

        Connection connection = getConnection();
        String sqlUpdate = "UPDATE teams SET name=? WHERE id=?";

        PreparedStatement ps = null;

        try {
            ps = connection.prepareStatement(sqlUpdate);
            ps.setString(1, team.getName());
            ps.setInt(2, team.getId());
            ps.executeUpdate();
            return true;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            close(ps);
            close(connection);
        }
        return false;
    }
}